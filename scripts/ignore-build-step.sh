#!/bin/bash

echo "VERCEL_ENV: $VERCEL_ENV"
echo "APPLICATION: $NX_APPLICATION"

if [[ $VERCEL == 1 ]]; then
  CURSOR="$VERCEL_GIT_COMMIT_SHA"
else
  CURSOR="HEAD"
fi

BASE="$CURSOR~1"

npm install

AFFECTED=$(npx nx print-affected --base=$BASE --select=projects)
echo "AFFECTED($BASE .. HEAD): $AFFECTED"
echo $AFFECTED | grep --quiet -E '(,| |^)'"$NX_APPLICATION"'(,| |$)'

if [[ $? == 0 ]]; then
  echo "✅ - Build can proceed"
  exit 1;
else
  echo "🛑 - Build cancelled"
  exit 0;
fi
