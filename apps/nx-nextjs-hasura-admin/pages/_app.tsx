import React from 'react';
import { AppProps } from 'next/app';
import Head from 'next/head';
import { ReactComponent as NxLogo } from '../public/nx-logo-white.svg';
import './styles.css';

function CustomApp({ Component, pageProps }: AppProps) {
  return (
    <>
      <Head>
        <title>Welcome to nx-nextjs-hasura-admin!</title>
      </Head>
      <div className="app">
        <header className="flex">
          <NxLogo width="75" height="50" />
          <h1>Welcome to nx-nextjs-hasura-admin!</h1>
        </header>
        <main>
          <h1>Making change here for admin for first time!!!</h1>
          <Component {...pageProps} />
        </main>
      </div>
    </>
  );
}

export default CustomApp;
